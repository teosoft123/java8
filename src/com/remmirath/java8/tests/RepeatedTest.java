package com.remmirath.java8.tests;

import static org.junit.Assert.assertEquals;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;

import org.junit.Test;

import com.remmirath.java8.annotations.Schedule;

@Schedule(hour = 2)
@Schedule(hour = 5)
public class RepeatedTest {

	@Test
	public void mustFindAnnotations() {
		Annotation[] annotations = AnnotatedElement.class.cast(
				RepeatedTest.class).getAnnotationsByType(Schedule.class);

		assertEquals(2, annotations.length);

		assertEquals(2, getSchedule(annotations[0]).hour());
		assertEquals(5, getSchedule(annotations[1]).hour());

	}

	private Schedule getSchedule(Annotation annotation) {
		return Schedule.class.cast(annotation);
	}

}
